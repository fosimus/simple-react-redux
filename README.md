# Simple React App

This is a simple React web application containing three routes:

1. Home page:
    * The content of the page will be added soon!
2. Posts page:
    * There is a list of posts from the JSONPlaceholder API.
    * The posts can be sorted alphabetically.
    * The posts can be grouped by username.
3. Audio page:
    * There are two different audio samples.
    * There is an opportunity to play the samples independently, or simultaneously.
    
## Technical scope
* React 16;
* Redux 3;
* React-Router 4;
* Webpack 4;
* Flexbox for views;
* Eslint with airbnb rules;
* etc (check package.json)

## Running (Node v6.11.5 required)


```
# Install node modules
yarn
```


```
# Start dev server on localhost:8080
yarn start
```


```
# Build dev version
yarn dev
//if you want to test it use simple server such as python:
cd dist && python -m SimpleHTTPServer 8000
```


```
# Build prod version
yarn build
# if you want to test it use some simple server such as python:
cd dist && python -m SimpleHTTPServer 8000
```

![gif](https://thumbs.gfycat.com/RecentLastAmericantoad-size_restricted.gif)