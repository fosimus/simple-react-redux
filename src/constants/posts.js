export const ADD_POSTS = 'postsPage/ADD_POSTS';
export const START_LOADING = 'postsPage/START_LOADING';
export const STOP_LOADING = 'postsPage/STOP_LOADING';
export const SET_SORTING = 'postsPage/SET_SORTING';
export const SET_USER = 'postsPage/SET_USER';
export const ADD_USERS = 'postsPage/ADD_USERS';
