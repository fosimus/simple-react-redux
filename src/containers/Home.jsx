import React from 'react';
import Panel from '../components/Panel/simple';

const Home = () => (
  <div>
    <Panel title="Nothing to see yet..." />
    <div className="no-elements">
      Explore songs and find amazing artists to follow!
    </div>
  </div>
);

export default Home;
