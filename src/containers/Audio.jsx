import React from 'react';
import { connect } from 'react-redux';
import Audio from '../components/Audio';
import Panel from '../components/Panel/simple';

const AudioPage = props => (
  <div>
    <Panel title="Discover New Music" />
    <Audio {...props} />
  </div>
);

const mapStateToProps = state => {
  const { samples } = state.audioPage;

  return {
    samples
  };
};

export default connect(mapStateToProps)(AudioPage);
