import React from 'react';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as actions from '../actions';
import Posts from '../components/Posts';
import Panel from '../components/Panel';

const PostsPage = props => (
  <div>
    <Panel
      title="Discover New Posts"
      isSorted={props.isSorted}
      selectedItem={props.selectedUser}
      items={props.users}
      onSortClick={props.actions.onSortClick}
      onSelectChange={props.actions.onUserNameChange}
    />
    <Posts {...props} />
  </div>
);

PostsPage.propTypes = {
  isSorted: PropTypes.bool,
  selectedUser: PropTypes.number,
  users: PropTypes.array,
  title: PropTypes.string,
  actions: PropTypes.object,
  onSortClick: PropTypes.func,
  onUserNameChange: PropTypes.func
};

PostsPage.defaultProps = {
  isSorted: false,
  selectedUser: '',
  users: [],
  title: '',
  actions: {},
  onSortClick: () => null,
  onUserNameChange: () => null
};

const getPosts = ({ isSorted, selectedUser }) => posts =>
  posts
    .filter(post => {
      if (selectedUser > 0) {
        return post.userId === selectedUser;
      }
      return true;
    })
    .sort((a, b) => {
      if (isSorted) {
        if (a.title < b.title) return -1;
        if (a.title > b.title) return 1;
      }
      return 0;
    });

const mapStateToProps = state => {
  const { isLoading, isSorted, selectedUser, users } = state.postsPage;

  return {
    isLoading,
    isSorted,
    selectedUser,
    users,
    posts: getPosts({
      isSorted,
      selectedUser
    })(state.postsPage.posts)
  };
};

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(actions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(PostsPage);
