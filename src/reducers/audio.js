const initialState = {
  samples: [
    {
      id: 1,
      name: '[Some group] New wave kit',
      src: 'https://static.bandlab.com/soundbanks/previews/new-wave-kit.ogg',
      type: 'audio/ogg'
    },
    {
      id: 2,
      name: '[LP] Synth organ',
      src: 'https://static.bandlab.com/soundbanks/previews/synth-organ.ogg',
      type: 'audio/ogg'
    }
  ]
};

export default function audios(state = initialState, action) {
  switch (action.type) {
    default:
      return state;
  }
}
