import audio from './audio';
import posts from './posts';

export default {
  audioPage: audio,
  postsPage: posts
};
