import * as types from '../constants/posts';

const initialState = {
  isLoading: false,
  isSorted: false,
  selectedUser: 0,
  users: [],
  posts: []
};

export default function posts(state = initialState, action) {
  switch (action.type) {
    case types.ADD_POSTS:
      return {
        ...state,
        posts: action.posts
      };
    case types.START_LOADING:
      return {
        ...state,
        isLoading: true
      };
    case types.STOP_LOADING:
      return {
        ...state,
        isLoading: false
      };
    case types.SET_SORTING:
      return {
        ...state,
        isSorted: !state.isSorted
      };
    case types.ADD_USERS:
      return {
        ...state,
        users: action.users
      };
    case types.SET_USER:
      return {
        ...state,
        selectedUser: Number(action.value)
      };

    default:
      return state;
  }
}
