import axios from 'axios';
import { toastr } from 'react-redux-toastr';
import * as types from '../constants/posts';
import { postsLink } from '../constants/config';

export const startLoading = () => ({ type: types.START_LOADING });
export const stopLoading = () => ({ type: types.STOP_LOADING });
export const addPosts = posts => ({ type: types.ADD_POSTS, posts });
export const addUsers = users => ({ type: types.ADD_USERS, users });
export const onSortClick = () => ({ type: types.SET_SORTING });
export const onUserNameChange = value => ({ type: types.SET_USER, value });

const generateUsers = posts =>
  posts
    .map(post => post.userId)
    // remove the same id`s
    .reduce((p, c) => (p.includes(c) ? p : [...p, c]), []);

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

export const fetchPosts = () => async dispatch => {
  dispatch(startLoading());
  try {
    // just for loader :)
    await sleep(1500);
    const { data: posts } = await axios.get(postsLink);
    const users = generateUsers(posts);
    dispatch(addPosts(posts));
    dispatch(addUsers(users));
  } catch (error) {
    toastr.error(error.message || 'Error');
  } finally {
    dispatch(stopLoading());
  }
};
