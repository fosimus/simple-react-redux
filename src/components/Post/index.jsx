import React from 'react';
import PropTypes from 'prop-types';
import './styles.less';

const Post = ({ title, userId, body }) => (
  <div className="post">
    <div className="post_head">
      <div className="post_title">{title}</div>
      <div className="post_user">user: {userId}</div>
    </div>
    <div className="post_body">{body}</div>
  </div>
);

Post.propTypes = {
  title: PropTypes.string,
  userId: PropTypes.number,
  body: PropTypes.string
};

Post.defaultProps = {
  title: '',
  userId: '',
  body: ''
};

export default Post;
