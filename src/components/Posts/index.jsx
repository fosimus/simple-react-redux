import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Post from '../../components/Post';
import Loading from '../../components/Loading';
import './styles.less';

const PostsList = posts => {
  const isEmpty = posts.length === 0;
  return isEmpty ? (
    <div className="no-elements">No posts</div>
  ) : (
    <div className="posts">
      {posts.map(post => (
        <div className="posts_item" key={post.id}>
          <Post {...post} />
        </div>
      ))}
    </div>
  );
};

class Posts extends Component {
  componentDidMount() {
    const { actions } = this.props;
    actions.fetchPosts();
  }

  render() {
    const { isLoading, posts } = this.props;
    return isLoading ? <Loading /> : PostsList(posts);
  }
}

Posts.propTypes = {
  isLoading: PropTypes.bool,
  actions: PropTypes.object,
  posts: PropTypes.array
};

Posts.defaultProps = {
  isLoading: false,
  actions: {},
  posts: []
};

export default Posts;
