import React from 'react';
import PropTypes from 'prop-types';
import './styles.less';

const Player = ({ src, type, name }) => (
  <div className="sample">
    <div className="sample_title">{name}</div>
    <audio controls className="sample_player">
      <source src={src} type={type} />
    </audio>
  </div>
);

Player.propTypes = {
  src: PropTypes.string,
  type: PropTypes.string,
  name: PropTypes.string
};

Player.defaultProps = {
  src: '',
  type: '',
  name: ''
};

export default Player;
