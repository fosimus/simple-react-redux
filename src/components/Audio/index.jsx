import React from 'react';
import PropTypes from 'prop-types';
import Sample from '../../components/Sample';
import './styles.less';

const Audio = ({ samples }) => {
  const isEmpty = samples.length === 0;
  return isEmpty ? (
    <div className="no-elements">No samples</div>
  ) : (
    <div className="samples">
      {samples.map(sample => (
        <div className="samples_item" key={sample.id}>
          <Sample {...sample} />
        </div>
      ))}
    </div>
  );
};

Audio.propTypes = {
  samples: PropTypes.array
};

Audio.defaultProps = {
  samples: []
};

export default Audio;
