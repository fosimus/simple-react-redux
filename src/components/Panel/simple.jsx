import React from 'react';
import PropTypes from 'prop-types';
import './styles.less';

const Panel = ({ title }) => (
  <div className="panel">
    <div className="panel_title">
      <h1>{title}</h1>
    </div>
  </div>
);

Panel.propTypes = {
  title: PropTypes.string
};

Panel.defaultProps = {
  title: ''
};

export default Panel;
