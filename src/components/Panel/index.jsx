import React from 'react';
import PropTypes from 'prop-types';
import './styles.less';

const Panel = ({
  isSorted,
  selectedItem,
  items,
  title,
  onSortClick,
  onSelectChange
}) => (
  <div className="panel">
    <div className="panel_title">
      <h1>{title}</h1>
    </div>
    <div className="panel_tools">
      <input
        className={`${isSorted && 'active'} control`}
        type="button"
        value="Sort by title"
        onClick={onSortClick}
      />
      <select
        className="control"
        defaultValue={selectedItem}
        onChange={e => onSelectChange(e.target.value)}
      >
        <option value="">Group by</option>
        {items.map(item => (
          <option value={item} key={item}>
            {item}
          </option>
        ))}
      </select>
    </div>
  </div>
);

Panel.propTypes = {
  isSorted: PropTypes.bool,
  selectedItem: PropTypes.number,
  items: PropTypes.array,
  title: PropTypes.string,
  onSortClick: PropTypes.func,
  onSelectChange: PropTypes.func
};

Panel.defaultProps = {
  isSorted: false,
  selectedItem: '',
  items: [],
  title: '',
  onSortClick: () => null,
  onSelectChange: () => null
};

export default Panel;
