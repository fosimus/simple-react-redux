import React from 'react';
import './styles.less';

const Loading = () => (
  <div className="loader">
    <div className="loader_element" />
  </div>
);

export default Loading;
