import React from 'react';
import { NavLink as Link } from 'react-router-dom';
import './styles.less';

const Navigation = () => (
  <div className="nav">
    <div className="nav_wrapper">
      <Link activeClassName="active" exact className="nav_link" to="/">
        Home<span className="nav_link_border" />
      </Link>
      <div className="nav_separator" />
      <Link activeClassName="active" exact className="nav_link" to="/posts">
        Posts<span className="nav_link_border" />
      </Link>
      <div className="nav_separator" />
      <Link activeClassName="active" exact className="nav_link" to="/audio">
        Audio<span className="nav_link_border" />
      </Link>
      <div className="nav_separator" />
      <a
        className="nav_link"
        href="https://www.instagram.com/fos_andy/"
        target="_blank"
        rel="noopener noreferrer"
      >
        Instagram<span className="nav_link_border" />
      </a>
      <div className="nav_separator" />
      <a
        className="nav_link"
        href="https://www.facebook.com/ushakov.andrei"
        target="_blank"
        rel="noopener noreferrer"
      >
        Facebook<span className="nav_link_border" />
      </a>
      <div className="nav_separator" />
      <a
        className="nav_link"
        href="https://www.linkedin.com/in/ushakovandrey/"
        target="_blank"
        rel="noopener noreferrer"
      >
        Linkedin<span className="nav_link_border" />
      </a>
    </div>
  </div>
);

export default Navigation;
