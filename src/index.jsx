import React, { Fragment } from 'react';
import { render } from 'react-dom';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import ReduxToastr, { reducer as toastrReducer } from 'react-redux-toastr';
import { composeWithDevTools } from 'redux-devtools-extension';
import { createStore, applyMiddleware, combineReducers } from 'redux';
import createHistory from 'history/createBrowserHistory';
import { Route } from 'react-router';
import {
  ConnectedRouter,
  routerReducer,
  routerMiddleware
} from 'react-router-redux';
import Navigation from './components/Navigation';
import Home from './containers/Home';
import Posts from './containers/Posts';
import Audio from './containers/Audio';
import Content from './components/Content';
import reducers from './reducers';
import './index.less';
import './libs/toastr.less';

const history = createHistory();

const store = createStore(
  combineReducers({
    ...reducers,
    toastr: toastrReducer,
    routing: routerReducer
  }),
  composeWithDevTools(applyMiddleware(routerMiddleware(history), thunk))
);

render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <Fragment>
        <Navigation />
        <Content>
          <Route exact path="/" component={Home} />
          <Route path="/posts" component={Posts} />
          <Route path="/audio" component={Audio} />
        </Content>
        <ReduxToastr />
      </Fragment>
    </ConnectedRouter>
  </Provider>,
  document.getElementById('app')
);
